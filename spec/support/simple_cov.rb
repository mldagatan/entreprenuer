SimpleCov.start 'rails' do
  add_filter '/bin/'
  add_filter '/db/'
  add_filter '/spec/'
  add_filter 'app/channels'
  add_filter 'app/controllers'

  add_group 'Form Objects', 'app/forms'
  add_group 'Presenters', 'app/presenters'
  add_group 'Service Objects', 'app/service'
end
