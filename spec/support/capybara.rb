Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.javascript_driver = :chrome

Capybara.configure do |capybara|
  capybara.default_max_wait_time = 10
  capybara.default_driver = :selenium
end
