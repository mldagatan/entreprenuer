import jQuery from 'jquery';
import Rails from 'rails-ujs';
import onmount from 'onmount';
import toastr from 'toastr';
import * as ActiveStorage from 'activestorage';
import './react';

require('foundation-sites');

window.$ = jQuery;
window.jQuery = jQuery;
window.toastr = toastr;

Rails.start();
ActiveStorage.start();

function requireAll(r) {
  r.keys().forEach(r);
}

requireAll(require.context('./behaviors/', true, /\.js$/));

document.addEventListener(
  'DOMContentLoaded',
  () => {
    onmount();
    $(document).foundation();
  },
  false
);
